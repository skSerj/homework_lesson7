package com.sourceIT.task2.models;

import com.sourceIT.task2.data.Generator;

public class Literature {
    private String name;
    private int yearOfPublishing;

    public Literature(String name, int yearOfPublishing) {
        this.name = name;
        this.yearOfPublishing = yearOfPublishing;
    }

    public boolean isYearOfPublishing(int desiredYearOfPublishing) {
        return yearOfPublishing == desiredYearOfPublishing;
    }

    @Override
    public String toString() {
        return String.format(" название: %s , год публикации: %s", name, yearOfPublishing);
    }

    public void findLitteratureByType(String desiredLiterature) {
        System.out.println(String.format(" название: %s , год публикации: %s", name, yearOfPublishing));
    }
}

