package com.sourceIT.task2.models;

public class Magazine extends Literature {
    private String topicOfMagazine;
    private String monthOfPublishing;

    public Magazine(String name, int yearOfPublishing, String topic, String monthOfPublishing) {
        super(name, yearOfPublishing);
        this.topicOfMagazine = topic;
        this.monthOfPublishing = monthOfPublishing;
    }

    @Override
    public String toString() {
        return String.format(super.toString() + " тематика: %s , месяц выпуска: %s", topicOfMagazine, monthOfPublishing);
    }

    public void findLitteratureByType(String desiredLiterature) {
        String desiredTypeMagazine = "журнал";
        if (desiredLiterature.equalsIgnoreCase(desiredTypeMagazine)) {
            System.out.println(super.toString() + " " + String.format("тематика: %s , месяц публикации: %s", topicOfMagazine, monthOfPublishing));
        }
    }
}
