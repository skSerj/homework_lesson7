package com.sourceIT.task2.models;

public class Almanac extends Literature {
    private String topicOfAlmanac;
    private String publishingHouseOfAlmanac;

    public Almanac(String name, int yearOfPublishing, String topicOfAlmanac, String publishingHouseOfAlmanac) {
        super(name, yearOfPublishing);
        this.topicOfAlmanac = topicOfAlmanac;
        this.publishingHouseOfAlmanac = publishingHouseOfAlmanac;
    }

    @Override
    public String toString() {
        return String.format("Название ежегодника: %s , издательство: %s", topicOfAlmanac, publishingHouseOfAlmanac) + " " + super.toString();
    }

    public void findLitteratureByType(String desiredLiterature) {
        String desiredTypeAlmonac = "ежегодник";
        if (desiredLiterature.equalsIgnoreCase(desiredTypeAlmonac)) {
            System.out.println(super.toString() + " " + String.format("тематика ежегодника: %s , издательство: %s", topicOfAlmanac, publishingHouseOfAlmanac));
        }
    }
}
