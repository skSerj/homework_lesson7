package com.sourceIT.task2.models;

public class Book extends Literature {
    private String author;
    private String publishingHouseOfBook;

    public Book(String name, int yearOfPublishing, String author, String publishingHouseOfBook) {
        super(name, yearOfPublishing);
        this.author = author;
        this.publishingHouseOfBook = publishingHouseOfBook;
    }

    @Override
    public String toString() {
        return String.format(super.toString() + " автор: %s , издательство: %s", author, publishingHouseOfBook);
    }

    public void findLitteratureByType(String desiredLiterature) {
        String desiredTypeBook = "книга";
        if (desiredLiterature.equalsIgnoreCase(desiredTypeBook)) {
            System.out.println(String.format("Автор книги: %s , издательство: %s", author, publishingHouseOfBook) + " " + super.toString());
        }
    }
}
