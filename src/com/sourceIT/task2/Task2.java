package com.sourceIT.task2;

import com.sourceIT.task2.data.Generator;
import com.sourceIT.task2.models.Literature;

import java.util.Scanner;

public class Task2 {
    public void run() {

        Scanner scan = new Scanner(System.in);
        System.out.println("Какого года издания(публикации), выбранной Вами литературы, желаете найти?: ");
        int desiredYearOfPublishing = Integer.parseInt(scan.nextLine());
        System.out.println("Что вы желаете найти? (Книга, журнал или ежегодник) :");
        String desiredLiterature = scan.nextLine();

        Literature[] literature = new Generator().generate();

        for (Literature literratureByYear : literature) {
            if (literratureByYear.isYearOfPublishing(desiredYearOfPublishing)) {
                System.out.println(literratureByYear);
            }
        }

        System.out.println("-----------------------");

        for (Literature literatureByType : literature) {
            literatureByType.findLitteratureByType(desiredLiterature);
        }
    }
}
