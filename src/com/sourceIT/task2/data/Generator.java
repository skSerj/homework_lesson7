package com.sourceIT.task2.data;

import com.sourceIT.task2.models.Almanac;
import com.sourceIT.task2.models.Book;
import com.sourceIT.task2.models.Literature;
import com.sourceIT.task2.models.Magazine;

public class Generator {
    public Generator() {
    }

    public Literature[] generate() {
        Literature[] literature = new Literature[6];
        literature[0] = new Book("Властелин колец", 1954, "Дж. Толкин", "World of fantasy");
        literature[1] = new Book("Двенадцать стульев", 1928, "Е.Петров", "Веселимся Вмессте");
        literature[2] = new Magazine("За рулём", 2020, "Автомобили", "апрель");
        literature[3] = new Magazine("Как устроен мир", 2019, "Наука", "октябрь");
        literature[4] = new Almanac("Толковый словарь Ушакова", 2020, "Наука", "Мир в одном");
        literature[5] = new Almanac("Открытия года", 2018, "общее", "Чудо");

        return literature;
    }

}
