package com.sourceIT.task1.data;

import com.sourceIT.task1.model.Bank;
import com.sourceIT.task1.model.BlackMarket;
import com.sourceIT.task1.model.Exchanger;
import com.sourceIT.task1.model.FinancialOrganization;

public class GeneratorFinance {
    public GeneratorFinance(){
    }
    public FinancialOrganization[] generateFinance(){
        FinancialOrganization[] financialOrganizations = new FinancialOrganization[3];
        financialOrganizations[0] = new Bank("ПриватБанк", 27.5f, 150000.0f);
        financialOrganizations[1] = new Exchanger("Обменник: меняем всё и чуточку больше", 27.45f, 20000.0f);
        financialOrganizations[2] = new BlackMarket("чёрный рынок: У Ашота", 28.0f);

        return financialOrganizations;
    }

}


