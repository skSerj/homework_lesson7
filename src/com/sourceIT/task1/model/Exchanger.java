package com.sourceIT.task1.model;

public class Exchanger extends FinancialOrganization {
    private Float limitByUSD;

    public Exchanger(String nameOrganization, Float rateUSD, Float limitByUSD) {
        super(nameOrganization, rateUSD);
        this.limitByUSD = limitByUSD;
    }

    public void findOrganizationAndconvert(FinancialOrganization organization, String disiredOrganization, int amount) {
        String desiredExchanger = "обменник";
        if (disiredOrganization.equalsIgnoreCase(desiredExchanger)) {
            if (amount / super.getRateUSD() < limitByUSD) {
                super.findOrganizationAndconvert(organization, disiredOrganization, amount);
            } else {
                System.out.println("превышен лимит обменник: " + limitByUSD);
            }
        }
    }
}
