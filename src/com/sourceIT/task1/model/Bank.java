package com.sourceIT.task1.model;

import java.sql.SQLOutput;

public class Bank extends FinancialOrganization {
    private Float bankLimitByUAH;

    public Bank(String nameOrganization, Float rateUSD, Float bankLimitByUAH) {
        super(nameOrganization, rateUSD);
        this.bankLimitByUAH = bankLimitByUAH;
    }

    public void findOrganizationAndconvert(FinancialOrganization organization, String disiredOrganization, int amount) {
        String desiredBank = "банк";
        if (disiredOrganization.equalsIgnoreCase(desiredBank)) {
            if (amount < bankLimitByUAH) {
                super.findOrganizationAndconvert(organization, disiredOrganization, amount);
            } else {
                System.out.println("превышен лимит банка: " + bankLimitByUAH);
            }
        }
    }
}
