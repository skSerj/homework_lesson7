package com.sourceIT.task1.model;

public class FinancialOrganization {
    private String nameOrganization;
    private Float rateUSD;

    public FinancialOrganization(String nameOrganization, Float rateUSD) {
        this.nameOrganization = nameOrganization;
        this.rateUSD = rateUSD;
    }


    public Float getRateUSD() {
        return rateUSD;
    }

    @Override
    public String toString() {
        return String.format("К вашим услугам: %s , курс доллара: %.2f", nameOrganization, rateUSD);
    }

    public void findOrganizationAndconvert(FinancialOrganization organization, String disiredOrganization, int amount) {
        float amountInUSD = amount/rateUSD;
        System.out.println( String.format("ваши деньги :%.2f, %s",amountInUSD,toString()));
    }
}

