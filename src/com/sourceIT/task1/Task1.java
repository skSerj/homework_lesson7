package com.sourceIT.task1;

import com.sourceIT.task1.data.GeneratorFinance;
import com.sourceIT.task1.model.*;
import com.sourceIT.task2.data.Generator;

import java.util.Scanner;

public class Task1 {
    public void run() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите какую сумму в гривнах вы желаете обменять: ");
        int amount = Integer.parseInt(scanner.nextLine());
        System.out.println("в какой организации желаете произвести операцию обмена? (банк, обменник, чёрный рынок): ");
        String disiredOrganization = scanner.nextLine();

        FinancialOrganization[] financialOrganization = new GeneratorFinance().generateFinance();
        for (FinancialOrganization organization : financialOrganization) {
           organization.findOrganizationAndconvert(organization,disiredOrganization,amount);
        }
    }
}
